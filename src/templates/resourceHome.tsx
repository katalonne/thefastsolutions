import { graphql } from "gatsby"
import React, { FC, Fragment } from "react"
import { Header } from "../components/Header"
import { ResourcesHomeContent } from "../components/ResourcesHomeContent"
import { SEO } from "../components/SEO"
import { PageContent } from "../components/PageContent"
import useSidebar from "../hooks/useSidebar"

// @todo maybe find alternative type for data
const ResourceHome: FC<any> = ({ data, pageContext }) => {
  const { current: language } = useSidebar()
  const { relativePath } = data.file || {}
  const { html, excerpt, fields, frontmatter, timeToRead } = data.file
    ? data.file.post
    : ({} as any)

  const shiftLayout = Boolean(
    frontmatter
      ? frontmatter.recommended_reading || frontmatter.external_resources
      : ""
  )

  return (
    <Fragment>
      <SEO
        title={frontmatter ? frontmatter.title : ""}
        subCategory={language}
        description={excerpt}
      />
      <Header
        relativePath={relativePath}
        basePath="/resources"
        title={frontmatter ? frontmatter.title : ""}
        authors={fields ? fields.authors : ""}
        createdAt={frontmatter ? frontmatter.created_at : ""}
        timeToRead={timeToRead}
        shifted={shiftLayout}
      />
      <PageContent
        content={
          <ResourcesHomeContent language={pageContext.language} html={html} />
        }
        recommendedReading={frontmatter ? frontmatter.recommended_reading : ""}
        externalResources={frontmatter ? frontmatter.external_resources : ""}
      />
    </Fragment>
  )
}

export default ResourceHome

export const query = graphql`
  query ResourceHome($resourceType: String!, $entry: String!) {
    file(
      sourceInstanceName: { eq: $resourceType }
      relativeDirectory: { eq: $entry }
      base: { eq: "intro.md" }
    ) {
      relativePath
      post: childMarkdownRemark {
        html
        excerpt
        fields {
          authors {
            name
            hash
            avatar
          }
        }
        frontmatter {
          created_at
          title
          recommended_reading
          external_resources {
            text
            href
          }
        }
        timeToRead
      }
    }
  }
`
