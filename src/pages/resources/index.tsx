import React from "react"

import { HeaderBarebone } from "../../components/HeaderBarebone"
import { Link } from "../../components/Link"
import { PageContent } from "../../components/PageContent"
import { ResourcesList } from "../../components/ResourcesList"
import { SEO } from "../../components/SEO"

function ResourcesPage() {
  return (
    <>
      <SEO title="Resources" />
      <HeaderBarebone
        title="Resurse"
        content={
          <>
            <p>
              Aici puteti gasi cateva resurse informative despre tehnologiile
              moderne <Link to="/about">Despre noi</Link>.
            </p>
          </>
        }
      />

      <PageContent content={<ResourcesList />} />
    </>
  )
}

export default ResourcesPage
