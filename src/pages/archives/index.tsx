import React, { Fragment } from "react"

import { HeaderBarebone } from "../../components/HeaderBarebone"
import { PageContent } from "../../components/PageContent"
import { SEO } from "../../components/SEO"

function ArchivesPage() {
  return (
    <Fragment>
      <SEO title="Arhiva" />
      <HeaderBarebone title="Arhiva cercetarilor" />

      <PageContent
        content={<p>Aceasta este arhiva cu cercetarile companiei.</p>}
      />
    </Fragment>
  )
}

export default ArchivesPage
