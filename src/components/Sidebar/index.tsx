import React, { FC, HTMLAttributes, PropsWithChildren } from "react"
import Scrollbar from "react-perfect-scrollbar"
import "react-perfect-scrollbar/dist/css/styles.css"
import cx from "classnames"
import { ThemeToggler } from "../ThemeToggler"
import * as SC from "./styles"

interface IMenuItemProps {
  to: string
}

const MenuItem: FC<IMenuItemProps> = ({ children, to }) => {
  return (
    <SC.MenuItem to={to} activeClassName="active">
      {children}
    </SC.MenuItem>
  )
}

export const Sidebar: FC<PropsWithChildren<HTMLAttributes<HTMLDivElement>>> = (
  props
) => {
  const { children, ...restProps } = props

  return (
    <SC.SidebarWrapper {...restProps}>
      <Scrollbar>
        <SC.Title to="/">
          The
          <br />
          Fast
          <br />
          Solutions
        </SC.Title>
        <SC.Inner>
          {children}

          <SC.Menu className={cx({ "has-border": Boolean(children) })}>
            <MenuItem to="/about">Despre Noi</MenuItem>
            <MenuItem to="/rules">Reguli</MenuItem>
            <MenuItem to="/resources">Resurse</MenuItem>
            <MenuItem to="/archives">Arhiva</MenuItem>
          </SC.Menu>

          <ThemeToggler />
        </SC.Inner>
      </Scrollbar>
    </SC.SidebarWrapper>
  )
}
